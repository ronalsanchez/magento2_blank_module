<?php
namespace Born\WeAreAwesome\Block;

class Blank
    extends \Magento\Framework\View\Element\Template
{
    public function getTitle()
    {
        return "My Block";
    }
}
